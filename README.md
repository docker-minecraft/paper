# Paper
A Docker image for running Paper.

[![build status](https://gitlab.com/docker-minecraft/paper/badges/master/build.svg)](https://gitlab.com/docker-minecraft/paper/commits/master)

# Installation
Here are the available versions of Paper (See https://ci.destroystokyo.com/job/PaperSpigot):
* latest (1.12.2)
* 1.11.2
* 1.10.2
* 1.9.4
* 1.8.8

Let's say we're running the latest build of Paper, simply clone and build the image:

```
$ git clone git@gitlab.com:docker-minecraft/paper.git
$ cd paper
$ docker build -t docker-minecraft/paper:latest --build-arg PAPER_URL=https://ci.destroystokyo.com/job/PaperSpigot/lastSuccessfulBuild/artifact/paperclip.jar src
$ docker run -p 25577:25577 -e "JAVA_ARGS=-Xmx1G" -v $(pwd)/my-paper:/data -itd --name my-paper docker-minecraft/paper
$ docker attach my-paper
```